package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

    public static final String ACCOUNT_SID = "ACc5e4e492db925bc024533595507e94f5";
    public static final String AUTH_TOKEN = "204d9547ca54d5b9cfad04c08b9e7422";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380971437863"),
                        new PhoneNumber("+12563716652"), str).create();
    }
}